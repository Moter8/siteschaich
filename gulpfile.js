'use strict';

var gulp = require('gulp'),
    jade = require('gulp-jade'),
    stylus = require('gulp-stylus'),
    minifyCss = require('gulp-minify-css'),
    nib = require('nib'),
    rupture = require('rupture'),
    connect = require('gulp-connect');

gulp.task('copyfiles', function() {
  gulp.src('src/img/**')
    .pipe(gulp.dest('build/img'));
  gulp.src('src/js/**')
    .pipe(gulp.dest('build/js'));
  gulp.src('src/css/fonts/**')
    .pipe(gulp.dest('build/css/fonts'));
  gulp.src('src/data/**')
    .pipe(gulp.dest('build/data'));
});

gulp.task('jade', function () {
  gulp.src('src/templates/**/*.jade')
    .pipe(jade())
    .pipe(gulp.dest('build'))
});

gulp.task('styles', function () {
  gulp.src('src/style/main.styl')
    .pipe(stylus({
    use: [nib(), rupture()]
  }))
    .pipe(minifyCss({compatibility: 'ie8'}))
    .pipe(gulp.dest('build/css'))
    .pipe(connect.reload());
});

gulp.task('html', function () {
  gulp.src('build/**/*.html')
    .pipe(connect.reload());
});

gulp.task('watch', function () {
  gulp.watch('src/templates/**/*.jade', ['jade']);
  gulp.watch('build/**/*.html', ['html']);
  gulp.watch('src/**/*.styl', ['styles']);
  gulp.watch('src/img/**/*', ['copyfiles']);
});

gulp.task('connect', function() {
  connect.server({
    root: 'build',
    livereload: true,
    port: 8888
  });
});

gulp.task('build', ['jade','styles', 'copyfiles']);

gulp.task('default', ['copyfiles', 'jade', 'styles', 'watch', 'connect']);
